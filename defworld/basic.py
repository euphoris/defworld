""":mod:`defworld.basic` --- Basic types
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
from .exception import DoesNotMatch

__all__ = 'Context', 'Item', 'PseudoFact', 'PseudoItem', 'Val', 'Var'


class Context(dict):
    def __getitem__(self, key):
        try:
            return super(Context, self).__getitem__(key)
        except KeyError:
            val = key.val()
            self[key] = val
            return val

    def __repr__(self):
        return 'Context({})'.format(super(Context, self).__repr__())

    def copy(self):
        return Context(self)


class Item(object):
    def __init__(self, context, facts):
        self.context = context
        self.facts = facts

    def join(self, other):
        # variable resolution
        keys = set(self.context.keys()) & set(other.context.keys())
        for key in keys:
            if self.context[key] != other.context[key]:
                raise DoesNotMatch()

        context = self.context.copy()
        context.update(other.context.items())

        # union facts
        facts = self.facts.union(other.facts)

        return Item(context, facts)

    def copy(self):
        return Item(self.context.copy(), self.facts.copy())

    def __repr__(self):
        return 'Item({0},{1})'.format(self.context, self.facts)


class PseudoFact(object):
    pass

class PseudoItem(Item):
    def __init__(self):
        self.context = Context()
        self.facts = set()


class Var(object):
    def __init__(self, identifier, *constraints, **kwargs):
        self.identifier = identifier
        self.constraints = list(constraints)
        for constraint in self.constraints:
            if not callable(constraint):
                raise TypeError('constraints must be callable, but {0!r} is '
                                'not callable'.format(constraint))
        self.options = kwargs

    def __repr__(self):
        return '$' + self.identifier

    def __hash__(self):
        return hash(self.identifier)

    def __eq__(self, other):
        return self.identifier == other.identifier

    def substitute(self, context):
        return context[self]

    def val(self):
        return Val(*self.constraints, **self.options)


class Val(object):
    def __init__(self, *constraints, **kwargs):
        self.has_value = False
        self.value = None
        self.constraints = constraints
        for constraint in self.constraints:
            if not callable(constraint):
                raise TypeError('constraints must be callable, but {0!r} is '
                                'not callable'.format(constraint))
        self.options = kwargs

    def __eq__(self, other):
        if self is other:
            return True

        if self.has_value:
            return self.value == other
        else:
            for validate_constraint in self.constraints:
                if not validate_constraint(other):
                    return False
            self.has_value = True
            self.value = other
            return True

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        return '${%s}' % repr(self.value)

    def __add__(self, other):
        if isinstance(other, Val):
            return self.value + other.value
        else:
            return self.value + other

    def __sub__(self, other):
        if isinstance(other, Val):
            return self.value - other.value
        else:
            return self.value - other

    def __mul__(self, other):
        if isinstance(other, Val):
            return self.value * other.value
        else:
            return self.value * other

    def __div__(self, other):
        if isinstance(other, Val):
            return self.value / other.value
        else:
            return self.value / other
