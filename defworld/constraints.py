""":mod:`defworld.constraints` --- Builtin constraints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""

__all__ = 'Typed',


class Typed(object):
    """Type checker.

    :param type: type to check the given value
    :type type: :class:`type`
    :param exact: if it's ``True`` it tests only its type.  otherwise
                  it tests the type's direct/indirect subclasses.
                  default is ``False``
    :type exact: :class:`bool`

    """

    def __init__(self, type, exact=False):
        self.type = type
        self.exact = exact

    def __call__(self, value):
        if self.exact:
            return type(value) is self.type
        return isinstance(value, self.type)
