""":mod:`defworld.agent` --- Agents
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
from .patterns import Entity
from .rete.node import Kind, Memory, RuleNode

__all__ = 'Agent', 'WorkingMemory'


class WorkingMemory(object):
    """
    A working memory of an :class:`Agent`
    which stores :class:`.patterns.Fact` and :class:`.patterns.Entity`

    :param initial_facts:
    :type initial_facts: a :class:`list` of :class:`.patterns.Fact`
    """
    def __init__(self, initial_facts=[]):
        self.facts = list(initial_facts)
        self.entities = {fact.name: fact for fact in initial_facts
            if isinstance(fact, Entity)}

        self.appended = list(initial_facts)
        self.removed = set([])

    def append(self, fact):
        """ Append a Fact to the WorkingMemory

        :param fact: a pointer to PixelWand to compare
        :type fact: :class:`.patterns.Fact`
        """
        self.facts.append(fact)
        self.appended.append(fact)

    def remove(self, item):
        self.facts.remove(item)
        if isinstance(item, Entity):
            del self.entities[item.name]
        self.removed.add(item)

        try:
            self.appended.remove(item)
        except ValueError:
            pass

    def clear(self):
        self.facts = []
        self.entries = {}

        self.appended = []
        self.removed = set([])


class Agent(object):
    """
    A forward inference agent

    :type initial_facts: a :class:`list` of :class:`.pattern.Fact`
    :type rules: a :class:`list` of :class:`.patterns.Rule`
    """
    def __init__(self, initial_facts, rules):
        self.rules = rules
        self.agenda = Memory()
        self.new_agenda = []

        # load initial facts onto WM
        self.wm = WorkingMemory(initial_facts)

        self.kinds = Kind(self.wm)

        # build a RETE network
        for rule in self.rules:
            left = None
            for pattern in rule.patterns:
                left = pattern.rete(self.kinds, left)

            left.connect(RuleNode(left, self, rule))
        self.nodes = self.linearize()

    def linearize(self):
        """ Return linearized rete tree """
        candidates = [self.kinds.root]
        nodes = []
        while candidates:
            node = candidates.pop(0)
            nodes.append(node)
            for node in node.post_nodes:
                if node not in candidates:
                    candidates.append(node)
        return nodes

    def clear(self):
        self.agenda.clear()

        for alpha in self.alphas:
            alpha.clear()

        for beta in self.betas:
            beta.clear()

    def match(self):
        for node in self.nodes:
            node.activate()

        self.wm.appended = []
        self.wm.removed.clear()
        return self.agenda

    def activate(self):
        """
        Activate the rule on the top of agenda
        """
        try:
            rule_context = self.agenda[-1]
            for change in rule_context.changes:
                change.apply(self.wm)
            return rule_context

        except IndexError:
            return None

    def run(self, max_iter=3):
        log = []
        i = 0
        while True:
            self.match()
            rule = self.activate()

            if rule:
                log.append(rule)
            else:
                break

            i += 1
            if i >= max_iter:
                break

        return log
