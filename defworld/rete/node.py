""":mod:`defworld.rete.node` --- Rete network
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
from ..basic import Context, Item, PseudoFact
from ..exception import DoesNotMatch

__all__ = ('AlphaNode', 'BetaNode', 'FactNode', 'Kind', 'Memory', 'Node',
           'NotNode', 'OrBetaNode', 'RootNode', 'RuleContext', 'RuleNode')


class Memory(list):
    def clear(self):
        del self[:]

    def filter(self, facts):
        i = 0
        n = len(self)
        while i < n:
            if self[i].facts & facts:
                del self[i]
                n -= 1
            else:
                i += 1


class Node(object):
    def __init__(self):
        self.memory = Memory()
        self.post_nodes = set()
        self.appended = Memory()

    def connect(self, node):
        self.post_nodes.add(node)

    def remove_fact(self):
        self.memory.filter(self.removed)

    def activate(self):
        self.remove_fact()
        self.appended.clear()
        self._activate()

    def __hash__(self):
        return id(self)


class AlphaNode(Node):
    def __init__(self, pre, pattern):
        self.pattern = pattern
        super(AlphaNode, self).__init__()
        self.pre = pre
        self.removed = pre.removed
        pre.connect(self)

    def _activate(self):
        for item in self.pre.appended:
            fact = list(item.facts)[0]
            try:
                item = self.pattern.match(fact, item)
            except DoesNotMatch:
                pass
            else:
                self.appended.append(item)

        self.memory.extend(self.appended)

    def clear(self):
        self.memory.clear()

    def __repr__(self):
        return 'AlphaNode({0})'.format(repr(self.pattern))


class BetaNode(Node):
    def __init__(self, left, right):
        self.left = left
        self.left_memory = Memory(left.memory)
        left.connect(self)

        self.right = right
        self.right_memory = Memory(right.memory)
        right.connect(self)

        self.removed = left.removed
        super(BetaNode, self).__init__()

    def remove_fact(self):
        self.left_memory.filter(self.removed)
        self.right_memory.filter(self.removed)
        super(BetaNode, self).remove_fact()

    def _activate(self):
        self.left_memory = Memory(self.left.memory)

        parity_memory = [self.left_memory, self.right_memory]
        pres = [self.right, self.left]

        # join left and right nodes
        for i in range(2):
            for item1 in pres[i].appended:
                for item2 in parity_memory[i]:
                    try:
                        new_item = item1.join(item2)
                    except DoesNotMatch:
                        pass
                    else:
                        self.appended.append(new_item)

        self.memory.extend(self.appended)

        self.right_memory = Memory(self.right.memory)

    def clear(self):
        self.state = False


class OrBetaNode(Node):
    def __init__(self, alphas):
        for alpha in alphas:
            alpha.connect(self)
        self.alphas = alphas
        self.removed = alphas[0].removed
        super(OrBetaNode, self).__init__()

    def _activate(self):
        for alpha in self.alphas:
            self.appended.extend(alpha.appended)

        self.memory.extend(self.appended)


class NotNode(Node):
    def __init__(self, left, right, minimum):
        self.left = left # preceding pattern
        self.right = right # truth check
        self.minimum = minimum # min number of left to activate

        left.connect(self)
        right.connect(self)

        self.removed = left.removed

        self.pseudofact = PseudoFact()
        super(NotNode, self).__init__()

    def _activate(self):
        changed = (len(self.left.appended) > self.minimum
                   or len(self.removed) > 0 )

        if changed:
            pseudoitem = Item(Context(), set([self.pseudofact]))

            if len(self.right.memory) == 0:
                # Any fact dosen't match the pattern
                if isinstance(self.left, FactNode):
                    self.memory = Memory([pseudoitem])
                else:
                    self.memory = Memory(item.join(pseudoitem)
                        for item in self.left.appended)
            else: # Some fact match the pattern
                self.memory = Memory()
                # remove items previously activated
                self.removed.add(self.pseudofact)

            self.appended = self.memory


class RuleNode(Node):
    def __init__(self, pre, agent, rule):
        self.pre = pre
        self.removed = pre.removed
        self.agent = agent
        self.rule = rule
        super(RuleNode, self).__init__()

    def remove_fact(self):
        super(RuleNode, self).remove_fact()
        self.agent.agenda.filter(self.removed)

    def _activate(self):
        for item in self.pre.appended:
            rule_context = RuleContext(self.rule, item.context, item.facts)
            self.agent.agenda.append(rule_context)

    def __repr__(self):
        return repr(self.rule)


class RuleContext(object):
    def __init__(self, rule, context, facts):
        self.rule = rule
        self.context = context
        self.name = rule.name
        self.facts = facts

    @property
    def changes(self):
        for change in self.rule.changes:
            yield change.substitute(self.context)

    def __repr__(self):
        return 'RuleContext({0}, {1}, {2})'.format(self.rule, self.context, self.facts)


class FactNode(Node):
    def __init__(self, root, name):
        self.name = name
        self.root = root
        root.connect(self)
        self.removed = root.removed
        super(FactNode, self).__init__()

    def _activate(self):
        self.appended = Memory(item for item in self.root.appended
            if list(item.facts)[0].name == self.name)


class RootNode(FactNode):
    def __init__(self, wm):
        self.wm = wm
        self.post_nodes = set()
        self.appended = Memory()
        self.removed = wm.removed
        self.memory = Memory()

    def _activate(self):
        self.appended = Memory(
            [fact.default_item() for fact in self.wm.appended])


class Kind(object):
    """ Kind nodes manager """
    def __init__(self, wm):
        self.alphas = {}
        self.fact_nodes = {}
        self.root = RootNode(wm)

    def add(self, pattern):
        if pattern in self.alphas:
            alpha = self.alphas[pattern]
        else:
            if pattern.is_named():
                if pattern.name not in self.fact_nodes:
                    self.fact_nodes[pattern.name] = \
                        FactNode(self.root, pattern.name)
                pre = self.fact_nodes[pattern.name]
            else:
                pre = self.root

            alpha = AlphaNode(pre, pattern)
            self.alphas[pattern] = alpha

        return alpha
