from defworld.agent import Agent, WorkingMemory
from defworld.basic import Var
from defworld.patterns import (And, Assign, Entity, Exist, Fact, ForAll, Not,
                               Retract, Rule, Template, Or, Update)
from defworld.rete.node import BetaNode, OrBetaNode, Kind

from .rules import (boy, chair, give, me, monkey, monkey_rules, shop,
                    school_rules, yandere, yandere_rules)


def test_rete():
    agent = Agent( [], monkey_rules)
    assert len(agent.kinds.fact_nodes) == 2

    for node in agent.kinds.fact_nodes['chair'].post_nodes:
        if node.pattern['at'] == 'middle-room':
            chair_at_middle = node
            break
    else:
        assert False

    for node in agent.kinds.fact_nodes['monkey'].post_nodes:
        if node.pattern['at'] == 'middle-room':
            monkey_at_middle_on_floor = node
            break
    else:
        assert False

    assert len(monkey_at_middle_on_floor.post_nodes) == 1
    assert len(chair_at_middle.post_nodes) == 1

    beta, = monkey_at_middle_on_floor.post_nodes
    assert beta.left is chair_at_middle
    assert beta.right is monkey_at_middle_on_floor

    rule_node, = beta.post_nodes
    assert rule_node.rule.name == 'climb-on-chair'

    for node in agent.kinds.fact_nodes['monkey'].post_nodes:
        if node.pattern.name == 'monkey' and node.pattern['hungry']:
            break
    else:
        assert False


def test_pattern_hash():
    agent = Agent([], [
        Rule('a', [Fact('data', x=1)],[]),
        Rule('b', [Fact('data', x=1), Fact('data', y=2)],[])])
    assert len(agent.kinds.fact_nodes['data'].post_nodes) == 2 # x=1 & y=2


def test_agenda_1():
    agent = Agent(
        [monkey(hungry=True, has='bananas')],
        monkey_rules)

    [rule_msg] = agent.match()
    assert rule_msg.name == 'eat-bananas'

def test_agenda_2():
    agent = Agent(
        [monkey(at='door', on='floor'), chair(at='door')],
        monkey_rules)
    agent.match()

    agenda = [rule.name for rule in agent.agenda]
    expected =['walk-from-door-to-middle-room',
        'push-chair-from-door-to-middle-room']
    assert agenda == expected

def test_push_and_climb():
    agent = Agent(
        [monkey(at='door', on='floor'), chair(at='door')],
        monkey_rules)
    log = [k.name for k in agent.run()]
    assert len(log) == 3

    expected = [
        'push-chair-from-door-to-middle-room',
        'climb-on-chair',
        'grasp-bananas']
    assert log == expected

    assert len(agent.agenda) == 1


class TestModifyWM(object):
    def terms(self):
        human = Template('human')
        animal = Template('animal')
        fact = Var('fact')

        return human, animal, fact

    def test_assign(self):
        human, animal, fact = self.terms()

        agent = Agent([human(), animal()],
            [Rule('assign-fact', [
                Assign(fact, human())],[])])
        log = agent.run()

        context = log[0].context

        assert fact in context
        assert context[fact] == human()

    def test_retract(self):
        human, animal, fact = self.terms()

        agent = Agent([human(), animal()],
            [Rule('assign-fact', [
                Assign(fact, human())
                ],[
                Retract(fact)])
            ])

        assert human() in agent.wm.facts
        agent.run()
        assert human() not in agent.wm.facts

    def test_update(self):
        human, animal, fact = self.terms()

        agent = Agent([human(updated=False), animal()],
            [Rule('update-fact', [
                Assign(fact, human())
                ],[
                Update(fact, updated=True)])
            ])

        agent.run()
        for f in agent.wm.facts:
            if f.name == 'human':
                assert f['updated']
                break
        else:
            assert False


def test_passing_var():
    agent = Agent(
        [yandere(has='lunchbox'), boy(has=None)],
        yandere_rules)
    agent.run()
    event = give(from_='yandere', to_='boy', what='lunchbox')
    assert event in agent.wm.facts

def test_operator():
    agent = Agent(
        [shop(knows='problem'),
         me(have=['mone'])],
        school_rules)
    agent.run()
    assert 'money' not in agent.wm.entities['me']['have']

def test_passing_var_operator():
    agent = Agent(
        [boy(loves='girl'), yandere(hates=[])],
        yandere_rules)
    agent.match()
    assert agent.agenda[-1].name == 'hate-list'

    agent.run()
    assert 'girl' in agent.wm.entities['yandere']['hates']

def test_nice_boat():
    agent = Agent(
        [Entity('girl', loves='boy'), yandere(hates=[])],
        yandere_rules)
    agent.match()
    assert agent.agenda[-1].name == 'nice-boat'

    agent.run()
    assert 'girl' in agent.wm.entities['yandere']['hates']


def test_not():
    agent = Agent(
        [boy(loves='yandere'), yandere(hates=[])],
        yandere_rules)
    agent.match()
    assert agent.agenda == []


def test_not_pattern():
    facts = [
        Fact('person', name='Jane', is_a='scientist'),
        Fact('person', name='Jisu', is_a='programmer'),
        Fact('panda', name='mingming', is_a='animal')]
    no_lawyer = [Rule('no-lawyer', [Not(Fact('person', is_a='lawyer'))], [])]
    agent = Agent(facts, no_lawyer)
    agent.match()
    assert len(agent.agenda) == 1

    agent.wm.append(Fact('person', name='Sunhee', is_a='lawyer'))
    agent.match()
    assert len(agent.agenda) == 0


def test_and_rete():
    and_pattern = And(Fact('data', a=1), Fact('data', b=2))
    kinds = Kind(WorkingMemory())

    terminal = and_pattern.rete(kinds, None)
    assert isinstance(terminal, BetaNode)

    assert len(kinds.fact_nodes['data'].post_nodes) == 2


def test_or_rete():
    or_pattern = Or(
        Fact('data', a=1),
        Fact('data', b=2),
        Fact('hello', to='world'))
    kinds = Kind(WorkingMemory())

    terminal = or_pattern.rete(kinds, None)
    assert isinstance(terminal, OrBetaNode)

    assert len(kinds.fact_nodes['data'].post_nodes) == 2
    assert len(kinds.fact_nodes['hello'].post_nodes) == 1


class TestExist(object):
    def exist(self, rule):
        # exist
        agent = Agent([
            Fact('english', name='Hong'),
            Fact('math', name='Hong'),
            Fact('english', name='Yu')],
            [rule])

        assert len(agent.kinds.fact_nodes) == 2
        agent.match()
        assert 'exists' in (rc.name for rc in agent.agenda)

        # not exist
        agent = Agent([
            Fact('math', name='Hong'),
            Fact('english', name='Yu')],
            [rule])
        agent.match()
        assert len(agent.agenda) == 0

    def test_not_not_and(self):
        rule = Rule('exists', [
            Not(Not(And(
                Fact('english', name=Var('x')),
                Fact('math', name=Var('x')))))],
            [])
        self.exist(rule)

    def test_exist(self):
        rule = Rule('exists', [
            Exist(
                Fact('english', name=Var('x')),
                Fact('math', name=Var('x')))],
            [])
        self.exist(rule)

    def test_save_the_day(self):
        """ from CLIPS Basic Programming Guide, p. 55"""

        goal = Template('goal')
        hero = Template('hero')

        agent = Agent([
            goal(goal='save-the-day'),
            hero(name='Death Defying Man'),
            hero(name='Stupendous Man'),
            hero(name='Incredible Man')
            ],[
            Rule('save-the-day', [
                goal(goal='save-the-day'),
                Exist(hero(satus=None))
            ],[])])

        agent.match()
        assert len(agent.agenda) == 1
        assert 'save-the-day' == agent.agenda[0].name


class TestForAll(object):
    def test_not_and_not(self):
        rule = Rule('all-students-pass', [
            Not(And(
                Fact('student', name=Var('x')),
                Not(And(
                    Fact('english', name=Var('x')),
                    Fact('math', name=Var('x'))))))],
            [])
        self.forall(rule)

    def test_forall(self):
        rule = Rule('all-students-pass', [
            ForAll(
                Fact('student', name=Var('x')),
                Fact('english', name=Var('x')),
                Fact('math', name=Var('x')))],
            [])
        self.forall(rule)

    def forall(self, rule):
        agent = Agent([
            Fact('student', name='Hong'),
            Fact('english', name='Hong'),
            Fact('math', name='Hong'),
            Fact('math', name='Yu'),
            Fact('english', name='Yu')],
            [rule])

        agent.match()
        assert 'all-students-pass' in (rc.name for rc in agent.agenda)

        agent = Agent([
            Fact('student', name='Hong'),
            Fact('english', name='Hong'),
            Fact('math', name='Hong'),
            # Fact('math', name='Yu'), Yu fails in math
            Fact('english', name='Yu')],
            [rule])

        assert len(agent.agenda) == 0


def test_wm_append():
    wm = WorkingMemory()
    wm.append(Entity('a'))
    assert len(wm.facts) == 1
    assert len(wm.appended) == 1

def test_wm_remove():
    a = Entity('a')
    wm = WorkingMemory([a])
    wm.remove(a)
    assert len(wm.facts) == 0
    assert len(wm.removed) == 1

def test_wm_entity():
    wm = WorkingMemory()
    Entity('a', at='door', on='floor').apply(wm)
    assert 'a' in wm.entities

    Entity('a', at='middle').apply(wm)
    assert wm.entities['a']['at'] == 'middle'
    assert wm.entities['a']['on'] == 'floor'


def test_linearize():
    agent = Agent([], school_rules)
    nodes = agent.linearize()

    # 1 root, 4 facts, 9 alphas, 3 betas, 6 rules = 23 nodes
    assert len(nodes) == 23

    agent = Agent([], monkey_rules)
    nodes = agent.linearize()

    # 1 root, 2 facts, 8 alphas, 2 betas, 6 rules
    assert len(nodes) == 19

    agent = Agent([], yandere_rules)
    nodes = agent.linearize()

    # 1 root, 2 facts, 5 alphas, 2 betas, 3 rules
    assert len(nodes) == 13
