from defworld.agent import WorkingMemory
from defworld.basic import Item
from defworld.patterns import Template
from defworld.rete.node import AlphaNode, BetaNode, FactNode, Memory, RootNode


def test_node():
    data = Template('data')

    root_node = RootNode(WorkingMemory())
    fact_node = FactNode(root_node, 'data')

    alpha1 = AlphaNode(fact_node, data(a=1))

    alpha2 = AlphaNode(fact_node, data(a=2))

    beta = BetaNode(alpha1, alpha2)

    fact_node.appended = Memory(
        [Item({}, set([data(a=1)])), Item({}, set([data(a=2)]))])

    alpha1.activate()
    assert data(a=1) in alpha1.appended[0].facts

    alpha2.activate()
    assert data(a=2) in alpha2.appended[0].facts

    beta.activate()
    assert set([data(a=1), data(a=2)]) == beta.appended[0].facts
    assert len(beta.appended) == 1
